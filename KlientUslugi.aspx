﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="KlientUslugi.aspx.cs" Inherits="TJEE_Lab4_klientSOAP.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>DataAccessSOAP</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>Testowy klient usługi DataAccessSOAP</h1>
            Podaj parametr wywołania usługi:<br />
            <asp:TextBox runat="server" ID="data"/>
            <asp:Button runat="server" ID="button" Text="Testuj" OnClick="Testuj"/><br /><br />
            <b>Odpowiedź metody DostepKlienta:</b><br />
            <asp:Label runat="server" ID="resp_dost" /><br /><br />
            <b>Odpowiedź metody Odwiedziny:</b><br />
            <asp:Label runat="server" ID="resp_odw" />
        </div>
    </form>
</body>
</html>